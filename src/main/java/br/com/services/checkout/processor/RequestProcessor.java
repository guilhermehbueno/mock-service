package br.com.services.checkout.processor;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import br.com.services.mocks.body.BodyLoader;
import br.com.services.mocks.body.BodyParser;
import br.com.services.mocks.body.MustacheBodyParser;
import br.com.services.mocks.models.MockSpecification;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import spark.Request;
import spark.Response;

public class RequestProcessor {
	
	
	private static final BodyParser bodyParser = new MustacheBodyParser();
	
	public String process(MockSpecification spec, BodyLoader loader, Request request, Response response){
		try{
			JsonReader reader = new JsonReader(new StringReader((request.body()+"").trim()));
			reader.setLenient(true);
			Map<String, Object> paramsRequest = new Gson().fromJson(reader, new TypeToken<HashMap<String, Object>>() {}.getType());
			Map<String, Object> params = new HashMap<>();
			
			if(spec.getParams()!=null){
				params.putAll(spec.getParams());
			}
			
			if(paramsRequest!=null){
				params.putAll(paramsRequest);
			}
			
			request.params().forEach((k, v) -> params.put(k.replaceAll(":",""), v));
			
			System.out.println(params);
			
			
			String body = loader.load(spec.getBodyLocation());
			body = bodyParser.parse(body, params);
			body = body.replaceAll("\\s+", "").replaceAll(",]", "]").trim();
			System.out.println(body);
			response.type("application/json");
			return body;
		}catch(Exception exc){
			exc.printStackTrace();
			return "ERROR: "+exc.getMessage();
		}
	}
}