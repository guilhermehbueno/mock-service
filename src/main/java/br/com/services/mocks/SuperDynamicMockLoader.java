package br.com.services.mocks;

import static spark.Spark.put;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;
import br.com.services.mocks.body.AggregatedBodyLoader;
import br.com.services.mocks.body.BodyLoader;
import br.com.services.mocks.body.InMemoryBodyLoader;
import br.com.services.mocks.models.MockSpecification;
import br.com.services.mocks.models.MockSpecifications;
import br.com.services.mocks.specification.AggregatedSpecificationLoader;
import br.com.services.mocks.specification.SpecificationLoader;

public class SuperDynamicMockLoader implements Loader {

	private final SpecificationLoader specificationLoader = new AggregatedSpecificationLoader();
	private final BodyLoader bodyLoader = new AggregatedBodyLoader();
	private MockSpecifications specifications;

	public static void main(String[] args) {
		new SuperDynamicMockLoader();
	}

	public void enableInputMocks() {
		Spark.get("/", (request, response) -> {
			return "OK";
		});
		Spark.put("/mock", (request, response) -> {
			try {
				System.out.println("Calling mock");
				Gson gson = new Gson();
				MockSpecification specification = gson.fromJson(request.body(), MockSpecification.class);
				// this.reload(null);
				this.load(specification);
				this.enableInputMocks();
				this.enableCors();
				return "OK";
			} catch (Exception exc) {
				exc.printStackTrace();
				return exc.getMessage();
			}
		});
		System.out.println("Registered: PUT -> /mock");
	}

	public void enableCors() {
		Spark.before(new Filter() {
			public void handle(Request request, Response response) {
				System.out.println("### Filtrando...");
				String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
				if (accessControlRequestHeaders != null) {
					response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
				}

				String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
				if (accessControlRequestMethod != null) {
					response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
				}

				response.header("Access-Control-Allow-Origin", "*");
				response.header("Access-Control-Request-Method", "*");
				response.header("Access-Control-Allow-Credentials", "true");
				response.header("Access-Control-Allow-Headers",
						"Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
			}
		});
		Spark.options("*", (request, response) -> {
			System.out.println("Options");
			String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
			if (accessControlRequestHeaders != null) {
				response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
			}

			String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
			if (accessControlRequestMethod != null) {
				response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
			}
			return "CORS ENABLED";
		});
	}

	public SuperDynamicMockLoader() {
		super();
		if (System.getenv("PORT") != null) {
			Integer port = Integer.valueOf(System.getenv("PORT"));
			Spark.port(port);
		} else {
			Spark.port(1081);
		}

		enableInputMocks();
		put("/scenario/:scenarioId", (req, res) -> {
			try {
				String scenarioId = req.params(":scenarioId");
				Map<String, String> params = new HashMap<>();
				params.put("specification", scenarioId);
				this.reload(scenarioId);
				String response = "Mock with scenario: " + scenarioId + " was loaded.";
				System.out.println(response);
				enableCors();
				enableInputMocks();
				return response;
			} catch (Exception exc) {
				exc.printStackTrace();
				return "an error occurs.";
			}
		});
		System.out.println("Registered: PUT -> /scenario/:scenarioId");
	}

	@Override
	public void load(String specification) {
		if (specification == null) {
			return;
		}
		this.specifications = specificationLoader.load(specification);
		if (this.specifications == null) {
			return;
		}
		specifications.getSpecifications().forEach(spec -> {
			if (StringUtils.isNotEmpty(spec.getBody())) {
				InMemoryBodyLoader.add(spec.getBodyLocation(), spec.getBody());
			}
			load(spec);
		});
	}

	public void load(MockSpecification specification) {
		if (StringUtils.isNotEmpty(specification.getBody())) {
			InMemoryBodyLoader.add(specification.getBodyLocation(), specification.getBody());
		}
		MethodTypeRegister.valueOf(specification.getMethod()).register(specification, bodyLoader);
	}

	@Override
	public void load(String prefix, Map<String, String> params) {
		load(prefix);
	}

	@Override
	public void stop() {
		Spark.stop();
	}

	@Override
	public void init() {
		Spark.init();
	}

	private void reload(String specification) {
		List<String> remove = new ArrayList<>();
		Spark.getRoutePaths().forEachRemaining(path -> {
			if (!"/scenario/:scenarioId".equalsIgnoreCase(path) && !"*".equalsIgnoreCase(path)
					&& !"+/*paths".equalsIgnoreCase(path)) {
				System.out.println("cleaning " + path);
				remove.add(path);
			}
		});
		remove.forEach(path -> Spark.remove(path));
		load(specification);
	}
}