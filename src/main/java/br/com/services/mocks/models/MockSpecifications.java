package br.com.services.mocks.models;

import java.util.ArrayList;
import java.util.List;

public class MockSpecifications {
	
	List<MockSpecification> specifications = new ArrayList<>();

	public List<MockSpecification> getSpecifications() {
		return specifications;
	}

	public void setSpecifications(List<MockSpecification> specifications) {
		this.specifications = specifications;
	}

	@Override
	public String toString() {
		return "MockSpecifications [specifications=" + specifications + "]";
	}
}