package br.com.services.mocks.models;

import java.util.Map;

public class MockSpecification {
	
	private String path;
	private String method;
	private String body;
	private String bodyLocation;
	private String queryString;
	private int delay;
	private Map<String, Object> params;
	
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		if(method!=null){
			this.method = method.toUpperCase();
		}else{
			this.method = method;
		}
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getBodyLocation() {
		return bodyLocation;
	}
	public void setBodyLocation(String bodyLocation) {
		this.bodyLocation = bodyLocation;
	}
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	public Map<String, Object> getParams() {
		return params;
	}
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
	@Override
	public String toString() {
		return "MockSpecification [path=" + path + ", method=" + method + ", body=" + body + ", bodyLocation="
				+ bodyLocation + ", queryString=" + queryString + ", delay=" + delay + ", params=" + params + "]";
	}
}
