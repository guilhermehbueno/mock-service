package br.com.services.mocks.body;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

public class MustacheBodyParser implements BodyParser {

	private final MustacheFactory mf;

	public MustacheBodyParser() {
		super();
		mf = new DefaultMustacheFactory();
	}

	@Override
	public String parse(String body, Map<String, Object> params) {
		return parse(body, (Object) params);
	}

	@Override
	public String parse(String body, Object data) {
		StringWriter writer = new StringWriter();
		Mustache mustache = mf.compile(new StringReader(body), "example");
		mustache.execute(writer, data);
		return writer.toString();
	}
}