package br.com.services.mocks.body;

import org.apache.commons.lang3.StringUtils;

public class AggregatedBodyLoader implements BodyLoader {

	private final BodyLoader fileBodyLoader = new FileBodyLoader();
	private final BodyLoader classpathBodyLoader = new ClassPathBodyLoader();
	private final BodyLoader inMemory = new InMemoryBodyLoader();

	@Override
	public String load(String bodyName) {
		try {
			String body = inMemory.load(bodyName);
			if(StringUtils.isNotEmpty(body)){
				return body;
			}
			
			body = fileBodyLoader.load(bodyName);
			if (body == null) {
				body = classpathBodyLoader.load(bodyName);
			}
			return body;
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;
	}

	@Override
	public String load(String bodyName, String location) {
		try {
			String body = inMemory.load(bodyName, location);
			if(StringUtils.isNotEmpty(body)){
				return body;
			}
			
			body = fileBodyLoader.load(bodyName, location);
			if (body == null) {
				body = classpathBodyLoader.load(bodyName, location);
			}
			return body;
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;
	}
}
