package br.com.services.mocks.body;

import java.util.Map;

public interface BodyParser {
	
	String parse(String body, Map<String, Object> params);
	String parse(String body, Object data);
}
