package br.com.services.mocks.body;

public interface BodyLoader {
	
	public String load(String bodyName);
	public String load(String bodyName, String location);

}