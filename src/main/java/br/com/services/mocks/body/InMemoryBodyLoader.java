package br.com.services.mocks.body;

import java.util.HashMap;
import java.util.Map;

public class InMemoryBodyLoader implements BodyLoader{
	
	private static final Map<String,String> BODIES = new HashMap<>();

	@Override
	public String load(String bodyName) {
		return BODIES.get(bodyName);
	}

	@Override
	public String load(String bodyName, String location) {
		return load(bodyName);
	}
	
	public static void add(String location, String bodyContent){
		BODIES.put(location, bodyContent);
	}
}