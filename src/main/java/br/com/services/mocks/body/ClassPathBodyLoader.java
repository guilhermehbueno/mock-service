package br.com.services.mocks.body;

import java.io.IOException;
import java.text.Normalizer;

import org.apache.commons.io.IOUtils;

public class ClassPathBodyLoader implements BodyLoader{
	
	private String prefix = "fixtures/";
	
	public ClassPathBodyLoader(String prefix) {
		super();
		this.prefix = prefix;
	}
	
	public ClassPathBodyLoader() {
		super();
	}

	@Override
	public String load(String bodyName) {
		 String result = "";

	        ClassLoader classLoader = getClass().getClassLoader();
	        try {
	        	String mock= this.prefix+bodyName;
	        	System.out.println("Loading: "+mock);
	            result = IOUtils.toString(classLoader.getResourceAsStream(mock), "UTF-8");
	            return Normalizer.normalize(result, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return result;
	}

	@Override
	public String load(String bodyName, String location) {
		return load(bodyName);
	}
}