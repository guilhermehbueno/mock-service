package br.com.services.mocks.specification;

import br.com.services.mocks.models.MockSpecifications;

public class AggregatedSpecificationLoader implements SpecificationLoader {

	private final SpecificationLoader fileSpecificationLoader = new FileSpecificationLoader();
	private final SpecificationLoader classPathSpecificationLoader = new ClassPathSpecificationLoader();

	@Override
	public MockSpecifications load(String specificationName) {
		try {
			MockSpecifications specification = fileSpecificationLoader.load(specificationName);
			if (specification == null) {
				specification = classPathSpecificationLoader.load(specificationName);
			}
			return specification;
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;
	}

	@Override
	public MockSpecifications load(String specificationName, String location) {
		try {
			MockSpecifications specification = fileSpecificationLoader.load(specificationName, location);
			if (specification == null) {
				specification = classPathSpecificationLoader.load(specificationName, location);
			}
			return specification;
		} catch (Exception exc) {
			exc.printStackTrace();
		}
		return null;
	}
}
