package br.com.services.mocks.specification;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import com.google.gson.Gson;

import br.com.services.mocks.models.MockSpecifications;

public class ClassPathSpecificationLoader implements SpecificationLoader{

	@Override
	public MockSpecifications load(String specificationName) {
		String result = "";
        ClassLoader classLoader = getClass().getClassLoader();
        MockSpecifications fromJson = null;
        try {
        	String mock= "specifications/"+specificationName;
        	System.out.println("Loading: "+mock);
        	InputStream stream = classLoader.getResourceAsStream(mock);
        	if(stream == null){
        		return null;
        	}
            result = IOUtils.toString(stream, "UTF-8");
            fromJson = new Gson().fromJson(result, MockSpecifications.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
		return fromJson;
	}

	@Override
	public MockSpecifications load(String specificationName, String location) {
		return load(specificationName);
	}
}