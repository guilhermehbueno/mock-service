package br.com.services.mocks.specification;

import br.com.services.mocks.models.MockSpecifications;

public interface SpecificationLoader {
	
	public MockSpecifications load(String specificationName);
	public MockSpecifications load(String specificationName, String location);

}
