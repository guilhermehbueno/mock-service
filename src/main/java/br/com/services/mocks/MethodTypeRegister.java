package br.com.services.mocks;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

import br.com.services.checkout.processor.RequestProcessor;
import br.com.services.mocks.body.BodyLoader;
import br.com.services.mocks.models.MockSpecification;

enum MethodTypeRegister {
	GET {
		@Override
		void register(MockSpecification spec, BodyLoader loader) {
			get(spec.getPath(), (req, res) -> {
				if(spec.getDelay()>0){
					Thread.sleep(spec.getDelay());
				}
				return new RequestProcessor().process(spec, loader, req, res);
			});
			logRoute(spec);
		}
	},
	POST {
		@Override
		void register(MockSpecification spec, BodyLoader loader) {
			post(spec.getPath(), (req, res) -> {
				System.out.println("Delay: "+spec.getDelay());
				if(spec.getDelay()>0){
					System.out.println("Delay: "+spec.getDelay());
					Thread.sleep(spec.getDelay());
				}
				return new RequestProcessor().process(spec, loader, req, res);
			});
			logRoute(spec);
		}
	},
	DELETE {
		@Override
		void register(MockSpecification spec, BodyLoader loader) {
			delete(spec.getPath(), (req, res) -> {
				if(spec.getDelay()>0){
					Thread.sleep(spec.getDelay());
				}
				return new RequestProcessor().process(spec, loader, req, res);
			});
			logRoute(spec);
		}
	},
	PUT {
		@Override
		void register(MockSpecification spec, BodyLoader loader) {
			put(spec.getPath(), (req, res) -> {
				if(spec.getDelay()>0){
					Thread.sleep(spec.getDelay());
				}
				return new RequestProcessor().process(spec, loader, req, res);
			});
			logRoute(spec);
		}
	};

	abstract void register(MockSpecification spec, BodyLoader loader);
	static void logRoute(MockSpecification spec) {
		System.out.println("Registered: " + spec.getMethod() + " -> " + spec.getPath());
	}
}