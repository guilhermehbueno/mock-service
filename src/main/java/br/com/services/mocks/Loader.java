package br.com.services.mocks;

import java.util.Map;

public interface Loader {
	
	public void stop();
	public void init();
	public void load(String specification);
	public void load(String prefix, Map<String, String> params);

}
