# Specification

Os arquivos dentro do diretório 'specifications' são especificações de mock. Nestes arquivos são configurados:

Os paths, mocks, caminhos de mock, delays e parâmetros a serem passados para o mock, ex:

Hello!

```
#!json

{
  "specifications": [
  {
  	"path": "/hello",
  	"method": "GET",
  	"delay": 3000,
  	"bodyLocation": "response/dynamic.json",
  	"params": {
  		"sku": "1312"
  	}
  },
  {
  	"path": "/hello",
  	"method": "POST",
  	"bodyLocation": "response/dynamic.json"
  }
  ]
}
```







## Fixtures
São onde são configurados mocks mais complexos. Em qualquer mock é possível adicionar marcações do Mustache pare preencher o mock com parâmetros pré-fixados nos arquivos de especificação ou vindos do request.


```
#!json

{
 "message": "Hello World",
 "sku": {{sku}}
}
```

## Carregando especification:
PUT: https://mock-marketplace-services.herokuapp.com/scenario/<<nomedocenario.json>>
Ex: https://mock-marketplace-services.herokuapp.com/scenario/dynamic.json

Verificando a rota:
GET: https://mock-marketplace-services.herokuapp.com/<<path passado no specification>>

POST: https://mock-marketplace-services.herokuapp.com/<<path passado no specification>>

Ex:

Path: https://mock-marketplace-services.herokuapp.com/hello

Payload:
```
#!json

{sku:33333}
```


## Adicionando um mock
Ainda não está pronto o endpoint que adiciona automáticamente o mock sem precisar reiniciar a aplicação.
Por enquanto é necessário adicionar os arquivos físicos ao projeto e realizar o deploy do mesmo.

## Rodando localmente
Para rodar localmente é só executar a classe: SuperDynamicMockLoader